import 'dart:async';

import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert' as convert;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:intl/intl.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';


import 'InkWellDrawer.dart';

const mainColor = Color(0xfff1e83c5);
const nyekundu = Color(0xffe32e16);
const tabcolor = Color(0xffe1dede);
const nyeupe = Color(0xffffffff);
const listcolor = Color(0xffffbfbfd);
const kijani = Colors.green;
const gray = Color(0xffa9a9a9);
const nyeusi= Color(0xff000000);

String cdst="Fetching Location... ";
String jina=" ";
String address="Loading address... ";
String id="";
String lat=" ";
String lon=" ";
String alt=" ";
String town=" ";
String akishoni=" ";
String street=" ";
String fon=" ";
String emair=" ";
String nem=" ";
String lastCheck=" ";
ProgressDialog pr, prr;
LatLng _center;
String messo=" ";
String refreshh="0";
String mesho="No updates found in the last four hours";
String  company;
String usertokeni = '';
String clogo="http://alerts.p-count.org/k.png";
String alogo="http://alerts.p-count.org/dirLogos/rock_logo.PNG";

GoogleMapController controller;
int hesabu=0;
String defaultMessage = "Enchogu";
Set<Marker> markers;
LatLng currentLocation =
LatLng(-1.286389, 36.817223);
Marker m;
FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
String etokeni=" ";
List<ActiveAlerts> activeAlerts;
List<InactiveAlerts> inactiveAlerts;
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
new FlutterLocalNotificationsPlugin();

class ClientDashboard extends StatefulWidget {

  _ClientDashboard createState() => _ClientDashboard();

}


class  _ClientDashboard extends State< ClientDashboard> {



  @override
  void initState() {
    super.initState();
    _restore();
    _getUsers();
    _determinePosition();
    initPlatformState();

    const oneSec = const Duration(seconds:60);
    new Timer.periodic(oneSec, (Timer t) => setState(() {
      refreshh;
    }));
  }


  _restore() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    jina = prefs.getString('name');
    usertokeni = prefs.getString('token');
    nem= prefs.getString('SFPName');
    fon= prefs.getString('SFPTelephone');
    emair= prefs.getString('SFPEmailAddress');
   // clogo= prefs.getString('CompanyLogo');
    alogo= prefs.getString('AppLogo');
    id= prefs.getString('user_id');

    print("Aye enchogu, id ni "+id);
    _getUsers();
    setState(() {
      setState(() {
        OneSignal.shared.setExternalUserId(id);
        jina;
        lastCheck;
        usertokeni;
        nem;
        emair;
        fon;
      });
    });
  }
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    StreamSubscription<Position> positionStream = Geolocator.getPositionStream(desiredAccuracy: LocationAccuracy.best, timeInterval: 30000).listen(

            (position) async {
          print(DateTime.now());
          final coordinates = new Coordinates(position.latitude, position.longitude);
          setState(()
          {
            position;
            print("Sasa Antho");

            cdst= "("+position.latitude.toString() +", "+position.longitude.toString()+")"+position.altitude.toStringAsFixed(4);
            print(cdst);
            lat=position.latitude.toString();
            lon=position.longitude.toString();

          });
          setState(() async {
            var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
            var first = addresses.first;
            address=first.locality+", "+first.countryName;
            town=first.locality;
            street=first.addressLine;
          });

        });

    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permantly denied, we cannot request permissions.');
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return Future.error(
            'Location permissions are denied (actual value: $permission).');
      }
    }

    return await Geolocator.getCurrentPosition();
  }
  Future<void> _alert() async {

    if(akishoni=="Acknowledge")
    {
      lon=" ";
      lat=" ";
    }
    pr.show();
    print("sasa");
    String apiUrl = "http://sms.geeckoltd.com/mobile/V1/AcknowledgeAlert";
    Map<String, String> headers = {"Content-type": "application/json","Accept": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({"RecNo": id, "Latitude": lat, "Longitude": lon});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    print("Endmonth");
    print(id);

    print(jsonsDataString);
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      print("Endmonth");

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      setState(() { refreshh; });
      Fluttertoast.showToast(
          msg: "Reporting succesful!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }

  Future<List<ActiveAlerts>> _getUsers() async {

    String apiUrl = "http://sms.geeckoltd.com/mobile/V1/GetUserAlert";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({  "DeviceToken": etokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<ActiveAlerts> alerts = [];

      if (map['ActiveLIst'] != null) {
        print("sawasawa");
        activeAlerts = new List<ActiveAlerts>();
        map['ActiveLIst'].forEach((v) {


          ActiveAlerts alert = ActiveAlerts(v["RecNo"],v["SMS"],v["Datesent"], v["ContactId"], v["NeedToBeAcknowledged"].toString(), v["Acknoledged"].toString());

          alerts.add(alert);
          print("200 bob imepotea");
        });
        print(alerts.length);

        return alerts;

      }




    }
    else {
      print("no");
    }

  }
  Future<List<InactiveAlerts>> _getUserss() async {

    String apiUrl = "http://sms.geeckoltd.com/mobile/V1/GetUserAlert";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({  "DeviceToken": etokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<InactiveAlerts> alerts = [];

      if (map['InactiveList'] != null) {
        print("sawasawa");
        inactiveAlerts = new List<InactiveAlerts>();
        map['InactiveList'].forEach((v) {



          InactiveAlerts alert = InactiveAlerts(v["RecNo"],v["SMS"],v["Datesent"], v["ContactId"], v["NeedToBeAcknowledged"].toString(), v["Acknoledged"].toString());

          alerts.add(alert);
          print("200 bob imepotea");
        });
        print(alerts.length);

        return alerts;

      }





    }
    else {
      print("no");
    }

  }

  ListView _jobsListView(activeAlerts) {
    return ListView.builder(
      padding: EdgeInsets.only(bottom:120),
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      itemCount: hesabu,
      itemBuilder: (BuildContext context, int index) {
        return userList(context, index);
      },
    );
  }
  Widget userList(BuildContext context, int index) {

  }
  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    pr.style(
        message: 'Reporting...',
        borderRadius: 10.0,
        backgroundColor: Colors.grey.withOpacity(0.8),
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('hh:mm a').format(now);
    setState(() {
      formattedDate;
    });
    return Scaffold(
        drawer: InkWellDrawer(),
      backgroundColor: listcolor,

      appBar: AppBar(
        backgroundColor: mainColor,
        title: Container(


          height: MediaQuery.of(context).size.height * 0.19,
          width: MediaQuery.of(context).size.width,


            decoration: BoxDecoration(

              color: mainColor,

            ),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
              crossAxisAlignment: CrossAxisAlignment.center, //Center Row contents vertically,
            children: <Widget>[
              Expanded(
                child: Container(
                  height:50,
                  margin: const EdgeInsets.only(bottom: 30.0, top: 30.0, right:10),
                  child: Image.asset("assets/gicon.png"),
                ),
              ),

      Expanded(
        child: Container(
          height:50,
          margin: const EdgeInsets.only(bottom: 30.0, top: 30.0, right:10),
          child: Image(
            image: NetworkImage(clogo),
          ),
        ),
           ),
            ],
          ),




        ),
      ),
        body: SingleChildScrollView(
          child: Stack(

            children: <Widget>[

              Column(
                crossAxisAlignment: CrossAxisAlignment.center,

                children: <Widget>[
                  Container(height: 10, color: mainColor),
                  Container(height: 8, color: Colors.transparent),
                  Text(
                    jina,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color:nyeusi,
                        fontSize: MediaQuery
                            .of(context)
                            .size
                            .height / 27.5,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Microsoft Himalaya'
                    ),

                  ),
                  Container(height: 5, color: Colors.transparent),

                  Container(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child:  Text(
                            "Last Seen : ",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color:nyeusi,
                                fontFamily: 'Montserrat-Regular'

                            ),

                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            child:  Text(
                              formattedDate,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: nyeusi,
                                  fontFamily: 'Montserrat-Regular'

                              ),

                            ),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Container(
                            child:  Text(
                              address,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: nyeusi,
                                  fontFamily: 'Montserrat-Regular'

                              ),

                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(height: 5, color: Colors.transparent),

                  DefaultTabController(
                    length: 2,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          color: gray,

                          child: Material(
                            color: gray,
                            child: TabBar(
                              indicatorColor: Colors.red,
                              labelColor: Colors.white,
                              unselectedLabelColor: nyeusi,
                              indicator:BoxDecoration(

                            gradient: LinearGradient(
                            colors: [mainColor, mainColor]),
                        borderRadius: BorderRadius.only(
                          topRight: Radius.zero,
                          bottomLeft:  Radius.circular(15.0),
                          bottomRight: Radius.circular(15.0),
                        ),

                        color: Colors.redAccent),


                              tabs: [
                                Tab(child:  Text(
                                  "Active",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: MediaQuery
                                          .of(context)
                                          .size
                                          .height / 35,
                                      fontWeight: FontWeight.bold
                                  ),

                                ),
                                ),
                                Tab(child:  Text(
                                  "Inactive",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: MediaQuery
                                          .of(context)
                                          .size
                                          .height / 35,
                                      fontWeight: FontWeight.bold
                                  ),

                                ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          //Add this to give height
                          height: MediaQuery.of(context).size.height,
                          child: TabBarView(children: [
                            Container(
                              color: mainColor,
                              child: FutureBuilder(
                                future: _getUsers(),
                                builder: (BuildContext context, AsyncSnapshot snapshot){
                                  print(snapshot.data);
                                  if(snapshot.data == null){
                                    return Container(
                                      color:Colors.white,
                                        child: Center(
                                            child: Text("Loading...")
                                        )
                                    );
                                  } else {
                                    return
                                    Container(
                                      color: nyeupe,
                                      child: ListView.builder(
                                      padding: EdgeInsets.only(bottom:300, top:5),
                                      itemCount: snapshot.data.length,
                                      itemBuilder: (BuildContext context, int index) {
                                        return Container(
                                          color: mainColor,
                                            child: Card(
                                          color: tabcolor,
                                          margin: EdgeInsets.all(1.6),
                                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                          elevation: 4.0,
                                          child: Container(
                                              decoration: BoxDecoration(color: tabcolor,
                                                borderRadius: BorderRadius.circular(40),
                                              ),
                                              child: ListTile(
                                                contentPadding: EdgeInsets.symmetric(horizontal: 5.0),


                                                title: Container(
                                                  padding: EdgeInsets.only(right: 13.0, left:13.0),


                                                  child: Text(
                                                    snapshot.data[index].alertName,
                                                    style: TextStyle(
                                                        color:nyeusi,
                                                        fontFamily: 'Montserrat-Regular'

                                                    ),
                                                  ),

                                                ),

                                                // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),
                                                subtitle: (snapshot.data[index].shtate=="1")
                                                    ?   Container(height: 3, color: Colors.transparent)

                                                    : Container(
                                                    padding: EdgeInsets.all(0), //<- try add this
                                                    margin: EdgeInsets.only(top: 15.0, bottom:3),
                                                    height: 24,

                                                        child: Container(
                                                      margin: const EdgeInsets.only(left: 80.0, right: 80.0),
                                                      child:    new RaisedButton(
                                                        padding: EdgeInsets.all(0), //<- try add this
                                                        elevation: 3.0,
                                                        color: mainColor,
                                                        shape: RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(20.0),
                                                        ),
                                                        onPressed: () {
                                                          id= snapshot.data[index].index.toString();
                                                          if(snapshot.data[index].shtate.toString()=="0")
                                                            {
                                                              akishoni="Acknowledge";

                                                            }

                                                          _alert();
                                                        },
                                                        child: Column(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                          children: <Widget>[

                                                            Center(child: Text(
                                                              "Acknowledge",
                                                              style: TextStyle(
                                                                color: Colors.white,
                                                              ),

                                                            ),  ),

                                                          ],
                                                        ),
                                                      ),

                                                    )
                                                ),
                                              )
                                        ),
                                            ),
                                        );
                                      },
                                      ),
                                    );
                                  }
                                },
                              ),


                            ),
                            Container(
                              color: mainColor,
                              child: FutureBuilder(
                                future: _getUserss(),
                                builder: (BuildContext context, AsyncSnapshot snapshot){
                                  print(snapshot.data);
                                  if(snapshot.data == null){
                                    return Container(
                                        color:Colors.white,
                                        child: Center(
                                            child: Text("Loading...")
                                        )
                                    );
                                  }
                                  else {
                                    return
                                      Container(
                                        color: nyeupe,
                                        child: ListView.builder(
                                          padding: EdgeInsets.only(bottom:300, top:5),
                                          itemCount: snapshot.data.length,
                                          itemBuilder: (BuildContext context, int index) {
                                            return Container(
                                              color: mainColor,
                                              child: Card(
                                                color: tabcolor,
                                                margin: EdgeInsets.all(1.6),
                                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                                elevation: 4.0,
                                                child: Container(
                                                    decoration: BoxDecoration(color: tabcolor,
                                                      borderRadius: BorderRadius.circular(40),
                                                    ),
                                                    child: ListTile(
                                                      contentPadding: EdgeInsets.symmetric(horizontal: 5.0),


                                                      title: Container(
                                                        padding: EdgeInsets.only(right: 13.0, left:13.0),


                                                        child: Text(
                                                          snapshot.data[index].alertName,
                                                          style: TextStyle(
                                                              color:nyeusi,
                                                              fontFamily: 'Montserrat-Regular'

                                                          ),
                                                        ),

                                                      ),

                                                    )
                                                ),
                                              ),
                                            );
                                          },
                                        ),
                                      );
                                  }
                                },
                              ),


                            ),

                          ]),
                        ),
                      ],
                    ),
                  ),

                ],
              ),
            ],

          ),
        ),


    );
  }

  _textMe(String number) async {
    // Android
    String uri = "sms:$number";
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      // iOS
      String uri = "sms:$number";
      if (await canLaunch(uri)) {
        await launch(uri);
      } else {
        throw 'Could not launch $uri';
      }
    }
  }

  _launchCaller(String number) async {
    String url = "tel:$number";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}



class ActiveAlerts {
  String index;
  String alertName;
  String alertDate;
  String alertStatus;
  String action;
  String shtate;


  ActiveAlerts(this.index, this.alertName, this.alertDate, this.alertStatus,  this.action, this. shtate);

}
class InactiveAlerts {
  String index;
  String alertName;
  String alertDate;
  String alertStatus;
  String action;
  String shtate;

  InactiveAlerts(this.index, this.alertName, this.alertDate, this.alertStatus,  this.action, this. shtate);
}
Future<void> initPlatformState() async {


  print("Before set log");
  //OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);
  print("Before set user privacy");
  //OneSignal.shared.setRequiresUserPrivacyConsent(_requireConsent);
  OneSignal.shared.setExternalUserId(id);

  var settings = {
    OSiOSSettings.autoPrompt: false,
    OSiOSSettings.promptBeforeOpeningPushUrl: true
  };

  OneSignal.shared.setNotificationReceivedHandler((notification) {


  });

  OneSignal.shared
      .setNotificationOpenedHandler((OSNotificationOpenedResult result) {
    print("Payload only is"+result.notification.payload.jsonRepresentation().toString());

  });
  OneSignal.shared.setExternalUserId(id);

  OneSignal.shared
      .setSubscriptionObserver((OSSubscriptionStateChanges changes) {
    print("SUBSCRIPTION STATE CHANGED: ${changes.jsonRepresentation()}");
  });

  OneSignal.shared.setPermissionObserver((OSPermissionStateChanges changes) {
    print("PERMISSION STATE CHANGED: ${changes.jsonRepresentation()}");
  });
  OneSignal.shared.setExternalUserId(id);


  // NOTE: Replace with your own app ID from https://www.onesignal.com
  await OneSignal.shared
      .init("30a54049-8b9b-40e8-b921-af51440943ad", iOSSettings: settings);

  OneSignal.shared.consentGranted(true);
  OneSignal.shared
      .setInFocusDisplayType(OSNotificationDisplayType.notification);
  await OneSignal.shared.getPermissionSubscriptionState();
  bool requiresConsent = await OneSignal.shared.requiresUserPrivacyConsent();

}